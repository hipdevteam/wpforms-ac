<?php

namespace Hip\WPFAC;

class Provider extends \WPForms_Provider
{
	public function __construct ($plugin) {
		$this->name = 'ActiveCampaign';
		$this->version = $plugin['version'];
		$this->slug = 'activecampaign';
		$this->priority = 10;
		$this->icon = $plugin['url'] . '/assets/images/icon.png';

		add_action( 'wp_ajax_wpforms_provider_wpfac_ajax', [ $this, 'process_wpfac_ajax' ] );		
		
		parent::__construct();
	}

	public function process_wpfac_ajax() 
	{
		check_ajax_referer( 'wpforms-builder', 'nonce' );

		if ( ! wpforms_current_user_can() ) {
			wp_send_json_error( [
				'error' => esc_html__( 'You do not have permission', 'wpfac' )
			]);
		}

		if ( 'select_pipeline' == $_POST['task'] ) {

			if ( $_POST['pipeline'] == 0 ) {
				wp_send_json_success([ 'html' => '' ]);
				die();
			}

			$stages = $this->output_deal_stages(
				$_POST['connection_id'], 
				[ 'account_id' => $_POST['account_id'] ],
				$_POST['pipeline']
			);

			wp_send_json_success([ 
				'html' => $stages
			]);
		}

		die();
	}

	public function process_entry( $fields, $entry, $form_data, $entry_id ) 
	{
		if ( empty( $form_data['providers'][$this->slug]) ) return;
		
		$providers = get_option( 'wpforms_providers' );

		try {
			foreach( $form_data['providers'][$this->slug] as $connection ) {
				$ac = $this->api_connect( $connection['account_id'] );

				$pass = $this->process_conditionals( $fields, $entry, $form_data, $connection );
				if ( ! $pass ) {
					wpforms_log (
						__( 'Active Campaign stopped by conditional logic', 'wpfac' ),
						$fields,
						[
							'type'		=> [ 'provider', 'conditional_logic' ],
							'parent'	=> $entry_id,
							'form_id'	=> $form_data['id']
						]
					);
					continue;
				}

				$email_data = explode( '.', $connection['fields']['email'] );
				$contact['email'] = $fields[$email_data[0]]['value'];

				if ( !empty( $connection['fields']['fullname'] ) ) {
					$fullname_data = explode( '.', $connection['fields']['fullname'] );

					if ( array_key_exists( 'value', $fields[$full_name[0]]) ) {
						$fullname = explode ( ' ', $fields[$full_name[0]]['value'] );
						$contact['first_name'] = $fullname[0] ? $fullname[0] : '';
						$contact['last_name'] = $fullname[1] ? $fullname[1] : '';
					}
				} else {
					$name_data = explode( '.', $connection['fields']['firstname'] );

					$contact['first_name'] = $fields[$name_data[0]]['first'];
					$contact['last_name'] = $fields[$name_data[0]]['last'];
				}
				
				$list = $connection['list_id'];
				$listkey = "p[$list]";
				$statuskey = "status[$list]";
				$contact[$listkey] = $list;
				$contact[$statuskey] = 1;

				$phone_data = explode( '.', $connection['fields']['phone'] );
				$org_data = explode( '.', $connection['fields']['org'] );

				if ( !empty( $phone_data[0] ) )
					$contact['phone'] = $fields[$phone_data[0]]['value'];

				if ( !empty( $org_data[0] ) )
					$contact['orgname'] = $fields[$org_data[0]]['value'];
				
				$contact['tags'] = apply_filters( 'wpforms_process_smart_tags', $connection['options']['tags'], $form_data, $fields, $entry_id );
				
				// Add custom fields
				foreach ( $connection['fields'] as $key => $field ) {
					if ( $key == 'firstname'
						|| $key == 'lastname'
						|| $key == 'fullname'
						|| $key == 'phone'
						|| $key == 'org'
						|| $key == 'email'
					) continue;

					if ( !empty( $field ) ) {
						$data = explode( '.', $field);
						$contact["field[$key,0]"] = $fields[$data[0]]['value'];
					}
				}

				// Process API, retrieve the contact ID
				$contact_sync = $ac->api("contact/sync", $contact);

				if ( !(int)$contact_sync->success ) {
					wpforms_log(
						__('Error Processing Entry', 'wpfac' ),
						$e->getMessage(),
						['type' => [ 'provider', 'error' ] ]
					);
					continue;
				}

				$contact_id = (int)$contact_sync->subscriber_id;

				// Add to automation
				if ( array_key_exists('automation_toggle', $connection['options']) ) {
					$ac->api("automation/contact/add", [
						'contact_id'	=> $contact_id,
						'automation'	=> $connection['options']['automation']
					]);
				}

				// Create a deal
				if ( array_key_exists('create_deal_toggle', $connection['options']) ) {
					$deal_options = $connection['options']['deal_create'];
					$deal['title'] = apply_filters( 'wpforms_process_smart_tags', $deal_options['title'], $form_data, $fields, $entry_id );
					
					if ( !empty($deal_options['value']) )  {
						$deal['value'] = apply_filters( 'wpforms_process_smart_tags', $deal_options['value'], $form_data, $fields, $entry_id );
					} else {
						$deal['value'] = 0;
					}
					if ( !empty($deal_options['currency']) ) {
						$deal['currency'] = apply_filters( 'wpforms_process_smart_tags', $deal_options['currency'], $form_data, $fields, $entry_id );
					} else {
						$deal['currency'] = 'usd';
					}
					if ( !empty($deal_options['owner']) ) 
						$deal['owner'] = $deal_options['owner'];

					$deal['pipeline'] = $deal_options['pipeline'];
					$deal['stage'] = $deal_options['stage'];
					$deal['contactid'] = $contact_id;

					wpforms_log( 'Deal Info', $deal, ['type' => ['provider', 'error']]);

					$deal_process = $ac->api("deal/add", $deal);
					wpforms_log( 'Deal Processed', $deal_process, ['type' => ['provider', 'error']]);
				}
			}
		} catch ( Exception $e ) {
			wpforms_log(
				__('Error Processing Entry', 'wpfac' ),
				$e->getMessage(),
				['type' => [ 'provider', 'error' ] ]
			);
			
			return $this->error( $e->getMessage() );
		}
	}
	
	public function api_auth( $data = [], $form_id = '' )
	{
		$id = uniqid();
		$providers = get_option( 'wpforms_providers', [] );
		
		$providers[$this->slug][$id] = [
			'url'	=> trim($data['acurl']),
			'key'	=> trim($data['ackey']),
			'label'	=> sanitize_text_field($data['label']),
			'date'	=> time()
		];
		
		update_option( 'wpforms_providers', $providers );
		
		return $id;
	}
	
	public function api_connect( $account_id )
	{
		$providers = get_option('wpforms_providers');
		
		if ( !empty( $auth = $providers[$this->slug][$account_id] ) ) {
			return new \ActiveCampaign($auth['url'], $auth['key']);
		}
		
		return $this->error( __('Error Connecting to ActiveCampaign') );
	}
	
	public function api_lists( $connection_id = '', $account_id = '' ) 
	{
		$ac = $this->api_connect( $account_id );
		
		if ( is_wp_error( $ac ) ) {
			return $ac;
		}
		
		try {
			$lists = $ac->api('list/list', [ 
				'ids'						=> 'all', 
				'full'					=> 1,
				'global_fields'	=> 1
			]);
		} catch( Exception $e ) {
			wpforms_log(
				__('Error Retrieving Lists', 'wpfac' ),
				$e->getMessage(),
				['type' => [ 'provider', 'error' ] ]
			);
			
			return $this->error( $e->getMessage() );
		}
		
		return $lists;
	}

	public function api_user_list( $connected_id = '', $account_id = '' )
	{
		$providers = get_option('wpforms_providers');

		$params = [
			'api_key'		=> $providers[$this->slug][$account_id]['key'],
			'api_output'	=> 'json',
		];

		$query = '';
		foreach($params as $key => $value) $query .= urlencode($key) . '=' . urlencode($value) . '&';
		$query = rtrim($query, '& ');
		$api = rtrim( $providers[$this->slug][$account_id]['url'], '/ ');
		$api = $api . '/api/3/users' . '?' . $query;

		$request = curl_init($api);

		curl_setopt($request, CURLOPT_HEADER, 0); 
		curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

		$response = (string)curl_exec($request);
		curl_close($request);

		if ( !$response ) {
			die(var_dump( $query ));
		}

		$result = json_decode($response, true);

		return $result;
	}

	public function api_list_deals( $connection_id = '', $account_id = '', $prev = [], $page = 1 )
	{
		$ac = $this->api_connect( $account_id );

		if ( is_wp_error( $ac ) ) {
			return $ac;
		}

		try {
			$deals = $ac->api('deal/list', [ 'page' => $page ]);
		} catch( Exception $e ) {
			wpforms_log(
				__( 'Error Retrieving Deals', 'wpfac' ),
				$e->getMessage(),
				['type' => ['provider', 'error' ] ]
			);
		}

		## Gives a hard limit of 400 ( 20x20 ) deals
		if ( sizeof( $deals->deals ) == 20 && $page < 20 ) {
			return $this->api_list_deals( $connection_id, $account_id, array_merge($prev, $deals->deals), $page + 1 );
		}
		
		return array_merge( $prev, $deals->deals );
	}

	public function api_list_deal_stages( $connection_id = '', $account_id = '', $pipeline = '', $prev = [], $page = 1 )
	{
		if ( empty($pipeline) ) {
			return;
		}

		$ac = $this->api_connect( $account_id );

		if ( is_wp_error( $ac ) ) {
			return $ac;
		}
		
		try {
			$stages = $ac->api('deal/stage/list', [ 
				'filters[pipeline]' => $pipeline, 
				'page' => $page 
			]);
		} catch( Exception $e ) {
			wpforms_log(
				__( 'Error Retrieving Deal Stages', 'wpfac' ),
				$e->getMessage(),
				['type' => [ 'provider', 'error' ] ]
			);
		}

		$stages = get_object_vars($stages);
		$stages = clean_stages($stages);

		if ( sizeof( $stages ) == 25  && $page < 5 ) {
			return $this->api_list_deal_stages( $connection_id, $account_id, $pipeline, array_merge( $prev, $stages ), $page + 1 );
		}
		
		return array_merge( $prev, $stages );
	}

	public function api_list_pipelines( $connection_id = '', $account_id = '' )
	{
		$providers = get_option('wpforms_providers');

		$params = [
			'api_key'		=> $providers[$this->slug][$account_id]['key'],
			'api_output'	=> 'json',
		];

		$query = '';
		foreach($params as $key => $value) $query .= urlencode($key) . '=' . urlencode($value) . '&';
		$query = rtrim($query, '& ');
		$api = rtrim( $providers[$this->slug][$account_id]['url'], '/ ');
		$api = $api . '/api/3/dealGroups' . '?' . $query;

		$request = curl_init($api);

		curl_setopt($request, CURLOPT_HEADER, 0); 
		curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

		$response = (string)curl_exec($request);
		curl_close($request);

		if ( !$response ) {
			die(var_dump( $query ));
		}

		$result = json_decode($response, true);

		return $result;
	}

	public function api_list_automations( $connection_id = '', $account_id = '' )
	{
		$ac = $this->api_connect( $account_id );

		if ( is_wp_error( $ac ) ) {
			return $ac;
		}
		
		try {
			$automations = $ac->api('automation/list');
		} catch( Exception $e ) {
			wpforms_log(
				__('Error Retrieving Automations', 'wpfac' ),
				$e->getMessage(),
				['type' => ['provider', 'error' ] ]
			);
		}

		return $automations;
	}
		
	public function api_fields( $connection_id = '', $account_id = '', $list_id = '' ) 
	{
		// We use this instead of list/view because we want to capture all fields.
		$lists = $this->api_lists( $connection_id, $account_id );
		
		$contact = [
			[
				'title' 			=> 'Email',
				'type'				=> 'email',
				'id'					=> 'email',
				'isrequired'	=> 1,
				'descript'		=> ''
			],
			[
				'title' 			=> 'First Name',
				'type'				=> 'text',
				'id'					=> 'firstname',
				'isrequired'	=> 0,
				'descript'		=> ''
			],
			[
				'title' 			=> 'Last Name',
				'type'				=> 'text',
				'id'					=> 'lastname',
				'isrequired'	=> 0,
				'descript'		=> ''
			],
			[
				'title' 			=> 'Full Name',
				'type'				=> 'text',
				'id'					=> 'fullname',
				'isrequired'	=> 0,
				'descript'		=> 'Use only if you cannot access first and last name.'
			],
			[
				'title' 			=> 'Phone',
				'type'				=> 'tel',
				'id'					=> 'phone',
				'isrequired'	=> 0,
				'descript'		=> ''
			],
			[
				'title' 			=> 'Organization',
				'type'				=> 'text',
				'id'					=> 'org',
				'isrequired'	=> 0,
				'descript'		=> ''
			]
		];
		
		return array_merge( $contact, $lists->{$list_id}->{'fields'} );
	}
	
	public function output_auth() 
	{
		$providers = get_option( 'wpforms_providers' );
		$class = ! empty( $providers[ $this->slug ] ) ? 'hidden ' : '';
		
		$output = '<div class="wpforms-provider-account-add ' . $class . ' wpforms-connection-block">';
		$output .= sprintf( '<h4>%s</h4>', __( 'Add New Account', 'wpfac' ) );
		$output .= sprintf( '<input type="text" data-name="acurl" placeholder="%s %s" class="wpforms-required">', $this->name, __( 'AC URL', 'wpfac' ) );
		$output .= sprintf( '<input type="text" data-name="ackey" placeholder="%s %s" class="wpforms-required">', $this->name, __( 'API Key', 'wpfac' ) );
		$output .= sprintf( '<input type="text" data-name="label" placeholder="%s %s" class="wpforms-required">', $this->name, __( 'Label', 'wpfac' ) );
		$output .= sprintf( '<button data-provider="%s">%s</button>', $this->slug, __( 'Connect', 'wpfac' ) );
		$output .= '</div>';
		
		return $output;
	}
	
	public function integrations_tab_new_form()
	{
		echo '<p>';
		printf( '<input type="text" name="acurl" placeholder="%s" class="wpforms-required">', __( 'URL', 'wpfac' ) );
		printf( '<input type="text" name="ackey" placeholder="%s" class="wpforms-required">', __( 'API Key', 'wpfac' ) );
		printf( '<input type="text" name="label" placeholder="%s" class="wpforms-required">', __( 'Account Nickname', 'wpfac' ) );
		echo '</p>';
	}
	
	public function output_groups( $connection_id = '', $connection = [] )
	{
	}
	
	public function output_js( $connection_id = '', $connection = [] )
	{
		ob_start(); ?>

		<script>
			jQuery(document).ready( function($) {

				$('.options_toggle').each( function() {
					if ( this.checked ) {
						$(this).closest('.options-wrap').children('.params').show();
					} else {
						$(this).closest('.options-wrap').children('.params').hide();
					}
				});

				$('.options_toggle').change(function() {
					if ( this.checked ) {
						console.log('change');
						$(this).closest('.options-wrap').children('.params').show();
					} else {
						$(this).closest('.options-wrap').children('.params').slideUp();
					}
				});
			});
		</script>

		<?php return ob_get_clean();
	}

	public function output_deal_stages( $connection_id = '', $connection = [], $pipeline )
	{
		$stages = $this->api_list_deal_stages( $connection_id, $connection['account_id'], $pipeline );
		$output .= sprintf( '<p class="wpfac-stage-select-wrap"><label class="block">%s</label>',
			__( 'Stage', 'wpfac')
		);
		$output .= sprintf( '<select name="providers[%s][%s][options][deal_create][stage]" class="wpfac-stage-select">',
			$this->slug,
			$connection_id
		);

		foreach ($stages as $stage) {
			if ( !empty($stage->id) ) {
				$selected = ( $connection['options']['deal_create']['stage'] == $stage->id ) ? 'selected': '';
				$output .= '<option value="'
					. $stage->id 
					. '" ' . $selected . ' >'
					. $stage->title 
					. '</option>';
			}
		}
		$output .= '</select></p>';

		return $output;
	}
	
	public function output_lists( $connection_id = '', $connection = array() ) {

		if ( empty( $connection_id ) || empty( $connection['account_id'] ) ) {
			return '';
		}

		$lists = $this->api_lists( $connection_id, $connection['account_id'] );
		$selected = ! empty( $connection['list_id'] ) ? $connection['list_id'] : '';

		if ( is_wp_error( $lists ) ) {
			return $lists;
		}

		$output = '<div class="wpforms-provider-lists wpforms-connection-block">';

		$output .= sprintf( '<h4>%s</h4>', esc_html__( 'Select List', 'wpforms' ) );

		$output .= sprintf( '<select name="providers[%s][%s][list_id]">', $this->slug, $connection_id );

		$lists = get_object_vars($lists);
		
		if ( ! empty( $lists ) ) {
			foreach ( $lists as $list ) {
				if ( ! is_object($list) ) continue;

				$list = get_object_vars($list);
				if ( ! empty( $list['id']) ) {
					$output .= sprintf(
						'<option value="%s" %s>%s</option>',
						esc_attr( $list['id'] ),
						selected( $selected, $list['id'], false ),
						esc_attr( $list['name'] )
					);
				}
			}
		}

		$output .= '</select>';

		$output .= '</div>';

		return $output;
	}

	public function output_fields( $connection_id = '', $connection = array(), $form = '' ) {

		if ( empty( $connection_id ) || empty( $connection['account_id'] ) || empty( $connection['list_id'] ) || empty( $form ) ) {
			return '';
		}

		$provider_fields = $this->api_fields( $connection_id, $connection['account_id'], $connection['list_id'] );
		$form_fields     = $this->get_form_fields( $form );

		if ( is_wp_error( $provider_fields ) ) {
			return $provider_fields;
		}

		$output = $this->output_js( $connection_id, $connection );
		
		$output .= '<style>span.descript { font-style:italic; display: block; }</style>';
		$output .= '<div class="wpforms-provider-fields wpforms-connection-block">';

		$output .= sprintf( '<h4>%s</h4>', esc_html__( 'List Fields', 'wpforms' ) );

		// Table with all the fields.
		$output .= '<table>';

		$output .= sprintf( '<thead><tr><th>%s</th><th>%s</th></thead>', esc_html__( 'List Fields', 'wpforms' ), esc_html__( 'Available Form Fields', 'wpforms' ) );

		$output .= '<tbody>';

		foreach ( $provider_fields as $provider_field ) :
			
			if ( is_object($provider_field) ) $provider_field = get_object_vars($provider_field);
		
			$output .= '<tr>';

			$output .= '<td>';

			$output .= esc_html( $provider_field['title'] );
			if (
				! empty( $provider_field['isrequired'] ) &&
				$provider_field['isrequired'] == '1'
			) {
				$output .= '<span class="required">*</span>';
			}
			
			if ( ! empty( $provider_field['descript'] ) ) {
				$output .='<span class="descript">' . $provider_field['descript'] . '</span>';
			}

			$output .= '<td>';

			$output .= sprintf( '<select name="providers[%s][%s][fields][%s]">', $this->slug, $connection_id, esc_attr( $provider_field['id'] ) );

			$output .= '<option value=""></option>';

			$options = $this->get_form_field_select( $form_fields, $provider_field['type'] );

			foreach ( $options as $option ) {
				$value    = sprintf( '%d.%s.%s', $option['id'], $option['key'], $option['provider_type'] );
				$selected = ! empty( $connection['fields'][ $provider_field['id'] ] ) ? selected( $connection['fields'][ $provider_field['id'] ], $value, false ) : '';
				$output  .= sprintf( '<option value="%s" %s>%s</option>', esc_attr( $value ), $selected, esc_html( $option['label'] ) );
			}

			$output .= '</select>';

			$output .= '</td>';

			$output .= '</tr>';

		endforeach;

		$output .= '</tbody>';

		$output .= '</table>';

		$output .= '</div>';

		return $output;
	}
	
	public function output_create_deal_options( $connection_id = '', $connection = [] )
	{

		$output = '<div class="params">';
		$output .= sprintf(
			'<p><label for="%s_options_create_deal_title" class="block">%s <a class="toggle-smart-tag-display" href="#" data-type="all" data-fields=""><i class="fa fa-tags"></i><span>Show Smart Tags</span></a></label><input id="%s_options_create_deal_title" type="text" name="providers[%s][%s][options][deal_create][title]" value="%s"></p>',
			$connection_id,
			__( 'Title', 'wpfac' ),
			$connection_id,
			$this->slug,
			$connection_id,
			$connection['options']['deal_create']['title'] ? esc_attr($connection['options']['deal_create']['title']) : ''
		);

		$output .= sprintf(
			'<p><label for="%s_options_create_deal_value" class="block">%s <a class="toggle-smart-tag-display" href="#" data-type="all" data-fields=""><i class="fa fa-tags"></i><span>Show Smart Tags</span></a></label><input id="%s_options_create_deal_value" type="text" name="providers[%s][%s][options][deal_create][value]" value="%s"></p>',
			$connection_id,
			__( 'Value', 'wpfac' ),
			$connection_id,
			$this->slug,
			$connection_id,
			$connection['options']['deal_create']['value'] ? esc_attr($connection['options']['deal_create']['value']) : ''
		);

		$output .= sprintf(
			'<p><label for="%s_options_create_deal_currency" class="block">%s <a class="toggle-smart-tag-display" href="#" data-type="all" data-fields=""><i class="fa fa-tags"></i><span>Show Smart Tags</span></a></label><input id="%s_options_create_deal_currency" type="text" name="providers[%s][%s][options][deal_create][currency]" value="%s"></p>',
			$connection_id,
			__( 'Currency', 'wpfac' ),
			$connection_id,
			$this->slug,
			$connection_id,
			$connection['options']['deal_create']['currency'] ? esc_attr($connection['options']['deal_create']['currency']) : 'usd'
		);

		$users = $this->api_user_list( $connection_id, $connection['account_id'] );
		$users = $users['users'];

		$output .= sprintf( '<p><label class="block">%s</label>',
			__( 'Owner', 'wpfac' )
		);

		$output .= sprintf( '<select name="providers[%s][%s][options][deal_create][owner]" class="wpfac-owner-select">',
			$this->slug,
			$connection_id
		);

		foreach( $users as $user ) {
			$selected = ( $connection['options']['deal_create']['owner'] == $user['id'] ) ? 'selected': '';
			$output .= '<option value="'
				. $user['id']
				. '" ' . $selected . ' >'
				. $user['firstName'] . ' ' . $user['lastName']
				. '</option>';
		}
		$output .= '</select></p>';

		$pipelines = $this->api_list_pipelines( $connection_id, $connection['account_id'] );
		$stages = $pipelines['dealStages'];
		$groups = $pipelines['dealGroups'];

		$output .= sprintf( '<p><label class="block">%s</label>',
			__( 'Pipeline', 'wpfac')
		);
		$output .= sprintf( '<select name="providers[%s][%s][options][deal_create][pipeline]" class="wpfac-pipeline-select">',
			$this->slug,
			$connection_id
		);
		

		foreach ($groups as $pipeline) {
			if ( !empty($pipeline['id']) ) {
				$selected = ( $connection['options']['deal_create']['pipeline'] == $pipeline['id'] ) ? 'selected': '';
				$output .= '<option value="'
					. $pipeline['id']
					. '" ' . $selected . ' >'
					. $pipeline['title']
					. '</option>';
			}
		}
		$output .= '</select></p>';

		$output .= sprintf( '<p><label class="block">%s</label>',
			__( 'Stage', 'wpfac')
		);
		$output .= sprintf( '<select name="providers[%s][%s][options][deal_create][stage]" class="wpfac-stage-select">',
			$this->slug,
			$connection_id
		);
		

		foreach ($stages as $stage) {
			if ( !empty($stage['id']) ) {
				$selected = ( $connection['options']['deal_create']['stage'] == $stage['id'] ) ? 'selected': '';
				$output .= '<option value="'
					. $stage['id']
					. '" ' . $selected . ' >'
					. $stage['title']
					. '</option>';
			}
		}
		$output .= '</select></p>';
		$output .= '</div>';

		return $output;
	}
	
	public function output_automation_options( $connection_id = '', $connection = [] )
	{
		$automations = $this->api_list_automations($connection_id, $connection['account_id']);
		$automations = get_object_vars($automations);
		$output = '<div class="params">';
		$output .= sprintf( '<select name="providers[%s][%s][options][automation]">',
			$this->slug,
			$connection_id);
		foreach ($automations as $automation) {
			if ( !empty($automation->id) ) {
				$selected = ( $connection['options']['automation'] == $automation->id ) ? 'selected' : '';
				$output .= '<option value="'
					. $automation->id 
					. '" '. $selected . ' >' 
					. $automation->name 
					. '</option>';
			}
		}
		$output .= '</select>';
		$output .= '</div>';
		
		return $output;
	}
	
	public function output_options( $connection_id = '', $connection = array() )
	{
		if ( empty( $connection_id ) || empty( $connection['account_id'] ) || empty( $connection['list_id'] ) )
			return;

		$tags = !empty( $connection['options']['tags'] ) ? esc_attr( $connection['options']['tags'] ) : '' ;
		
		$output = '<div class="wpforms-provider-options wpforms-connection-block">';

			$output .= sprintf( '<h4>%s</h4>', __( 'Options', 'wpfac' ) );

			$output .= sprintf(
							'<p><label for="%s_options_tags" class="block">%s <i class="fa fa-question-circle wpforms-help-tooltip" title="%s"></i><a class="toggle-smart-tag-display" href="#" data-type="all" data-fields=""><i class="fa fa-tags"></i><span>Show Smart Tags</span></a></label><input id="%s_options_tags" type="text" name="providers[%s][%s][options][tags]" value="%s"></p>',
							$connection_id,
							__( 'Tags', 'wpfac' ),
							__( 'Comma-separated list of tags to assign to contact in ActiveCampaign.', 'wpfac' ),
							$connection_id,
							$this->slug,
							$connection_id,
							$tags
						);
			
			$output .= '<div class="create-deal-wrap options-wrap">';
				$output .= sprintf('<p><input class="options_toggle" id="wpforms-panel-field-%s-%s-create-deal-toggle" name="providers[%s][%s][options][create_deal_toggle]" value="1" %s type="checkbox"><label class="inline">%s</label></p>', 
				$this->slug, 
				$connection_id, 
				$this->slug, 
				$connection_id,
				!empty( $connection['options']['create_deal_toggle']) ? "checked" : '', 
				__( 'Create a Deal', 'wpfac' ));
				$output .= $this->output_create_deal_options( $connection_id, $connection );
			$output .= '</div>';

			$output .= '<div class="automation-wrap options-wrap">';
				$output .= sprintf('<p><input class="options_toggle"  id="wpforms-panel-field-%s-%s-automation-toggle" name="providers[%s][%s][options][automation_toggle]" value="1" %s type="checkbox"><label class="inline">%s</label></p>', 
				$this->slug, 
				$connection_id, 
				$this->slug, 
				$connection_id,
				!empty( $connection['options']['automation_toggle']) ? "checked" : '', 
				__( 'Add to an Automation', 'wpfac' ));
				$output .= $this->output_automation_options( $connection_id, $connection );
			$output .= '</div>';
		$output .= '</div>';

		return $output;
	}
}
