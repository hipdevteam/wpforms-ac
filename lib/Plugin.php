<?php 

namespace Hip\WPFAC;
use Pimple\Container;

class Plugin extends Container
{
	public function __construct()
	{
	}
	
	public function run()
	{
		add_action( 'wpforms_loaded', function() { $this->provider = new Provider($this); } );
	}
}
