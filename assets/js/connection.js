// ;(function($) {
//     var s;
    
//     var WPFormsACProvider = {
//         settings: {
//             spinner: '<i class="fa fa-circle-o-notch fa-spin wpforms-button-icon" />'
//         },

//         init: function() {
//             s = this.settings;

//             $(document).ready(WPFormsACProvider.ready);
//             WPFormsACProvider.bindUIActions();
//         },

//         ready: function() {
//             s.formID = $('wpforms-builder-form').data('id');
//         },

//         bindUIActions: function() {

//             $(document).on('change', '.wpfac-pipeline-select', function(e) {
//                 WPFormsACProvider.pipelineSelect(this, e);
//             });
//         },

//         pipelineSelect: function(el, e) {
//             e.preventDefault();

//             var $this = $(el),
//             $connection = $this.closest('.wpforms-provider-connection'),
//             $container = $this.parent();
//             provider = $connection.data('provider');

//             WPFormsACProvider.inputToggle($this, 'disable');

//             // Remove the stage select
//             $container.nextAll('.wpfac-stage-select-wrap').remove();
            
//             data = {
//                 action: 'wpforms_provider_wpfac_ajax',
//                 provider: provider,
//                 connection_id: $connection.data('connection_id'),
//                 task: 'select_pipeline',
//                 account_id: $connection.find('.wpforms-provider-accounts option:selected').val(),
//                 pipeline: $this.find(':selected').val(),
//                 form_id: s.formID
                
//             };

//             WPFormsACProvider.fireAJAX($this, data, function(res) {
//                 if (res.success) {
//                    $container.after(res.data.html);
//                 } else {
//                     WPFormsACProvider.errorDisplay(res.data.error, $container);
//                 }
//             });
//         },

// 		inputToggle: function(el, status) {
// 			var $this = $(el);
// 			if (status == 'enable') {
// 				if ($this.is('select')) {
// 					$this.prop('disabled', false).next('i').remove();
// 				} else {
// 					$this.prop('disabled', false).find('i').remove();
// 				}
// 			} else if (status == 'disable'){
// 				if ($this.is('select')) {
// 					$this.prop('disabled', true).after(s.spinner);
// 				} else {
// 					$this.prop('disabled', true).prepend(s.spinner);
// 				}
// 			}
//         },
        
// 		errorDisplay: function(msg, location) {
// 			location.find('.wpforms-error-msg').remove();
// 			location.prepend('<p class="wpforms-alert-danger wpforms-alert wpforms-error-msg">'+msg+'</p>');
//         },
        
// 		fireAJAX: function(el, d, success) {
// 			var $this = $(el);
// 			var data = {
// 				id    : $('#wpforms-builder-form').data('id'),
// 				nonce : wpforms_builder.nonce
// 			}
// 			$.extend(data, d);
// 			$.post(wpforms_builder.ajax_url, data, function(res) {
// 				success(res);
// 				WPFormsACProvider.inputToggle($this, 'enable');
// 			}).fail(function(xhr, textStatus, e) {
// 				console.log(xhr.responseText);
// 			});
// 		}
//     };

//     WPFormsACProvider.init();
// })(jQuery);