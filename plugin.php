<?php 
/**
 * Plugin Name: Active Campaign for WPForms
 * Description: Integrates Active Campaign directly with WPForms.
 * Version: 0.1.1
 * Author: Zach
 * Text Domain: wpfac

============================================================================================================
This software is provided "as is" and any express or implied warranties, including, but not limited to, the
implied warranties of merchantibility and fitness for a particular purpose are disclaimed. In no event shall
the copyright owner or contributors be liable for any direct, indirect, incidental, special, exemplary, or
consequential damages(including, but not limited to, procurement of substitute goods or services; loss of
use, data, or profits; or business interruption) however caused and on any theory of liability, whether in
contract, strict liability, or tort(including negligence or otherwise) arising in any way out of the use of
this software, even if advised of the possibility of such damage.

For full license details see LICENSE.md
============================================================================================================
*/

if ( file_exists( __DIR__ . '/vendor/autoload.php' ) ) {
	require __DIR__ . '/vendor/autoload.php';
	define('WPFORMS_DEBUG', true);
}

add_action( 'plugins_loaded', function() {
	$plugin = new Hip\WPFAC\Plugin();
	$plugin['version'] = '0.1.1';
	$plugin['url'] = plugins_url( '', __FILE__ );
	$plugin['dir'] = plugin_dir_path( __FILE__ );
	
	$plugin->run();
} );
